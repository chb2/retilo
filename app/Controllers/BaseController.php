<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\HTTP\CLIRequest;
use CodeIgniter\HTTP\IncomingRequest;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;
use App\Models\InstallModel;
use App\Models\PageModel;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 */
abstract class BaseController extends Controller
{
    /**
     * Instance of the main Request object.
     *
     * @var CLIRequest|IncomingRequest
     */
    protected $request;
    
      protected $viewData = [];

    /**
     * An array of helpers to be loaded automatically upon
     * class instantiation. These helpers will be available
     * to all other controllers that extend BaseController.
     *
     * @var array
     */
   // protected $helpers = [];
    protected $helpers = ["url"];

    /**
     * Be sure to declare properties for any property fetch you initialized.
     * The creation of dynamic property is deprecated in PHP 8.2.
     */
    // protected $session;

    /**
     * Constructor.
     */
    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);


                // debut code à ajouter dans construc pour langue
                $session = \Config\Services::session();
                $userapp = $session->userapp;
                $lang= $session->lang ;
                
     
                //echo 'lang '.$lang;
              //  exit;

                if ($lang == ''){
                  $lang = 'en';
                  $session->lang = 'en';
                }
                      
                $language = \Config\Services::language();
                $language->setLocale($lang); 
                // fin code à ajouter dans construc pour langue
                

        // Preload any models, libraries, etc, here.

        $router = service('router');
        $controller  = $router->controllerName();
        $method = $router->methodName();
        
        // début code installation si aucun admin    
        $install = new InstallModel();
        $crtadmin = $install->getuzantoAdmin();
        if ($crtadmin == 0 and $method <> 'install-info' and $method <> 'partie1controle' and $method <> 'partie3installation')
        {
          $data['page'] = "install";
          $data['action'] = "install-info";
          $page = new PageModel();
          $page->page($data);
          exit;
        }
        // fin code installation si aucun admin   


        // E.g.: $this->session = \Config\Services::session();
        
        $this->viewData['locale'] = $request->getLocale();
        $this->viewData['supportedLocales'] = $request->config->supportedLocales;
    }
}
