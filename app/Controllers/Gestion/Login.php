<?php namespace App\Controllers\Gestion;
use \App\Controllers\BaseController;
use App\Models\PageModel;
use App\Models\LoginModel;
use App\Models\UzantoModel;

class Login extends BaseController
{
    public function index()
	{
        $data['view']['title'] = "Identification";
        $data['msgerr'] = "";
        $data['page'] = "login";
        $data['msgerr'] = "";
        $page = new PageModel();
        $page->gestion($data,'login');
	}

    public function logincheck()
	{
	   
     
        // debut code à ajouter dans construc pour langue
                       $session = \Config\Services::session();
                       $userapp = $session->userapp;
                       $lang= $session->lang ;
                       $language = \Config\Services::language();
                       $language->setLocale($lang); 
                       // fin code à ajouter dans construc pour langue
       
        $u = $this->request->getVar('u');
        $p = $this->request->getVar('p');
        $login = new LoginModel();
        $data = $login->logincheck($u,$p);
     }

    public function demchgpas1()
	{
        $data['msgerr'] = "Pour demander à changer votre mot de passe, vous devez avoir un email validé. Vous recevrez les instructions sur cet email";
        $page = new PageModel();
        $page->gestion($data,'login-dempas');
    }

    public function demchgpas2()
	{
        $p['email1'] = $this->request->getVar('email1');
        $uzanto = new UzantoModel();
        $uzanto->demchgpas2($p);
        $data['view']['title'] = "Changement mot de passe";
        $data['msgerr'] = "Un email vient de vous être envoyé";
        $data['page'] = "moncompte";
        $data['action'] = "motcompteinfo";
        $page = new PageModel();
        $page->affiche($data);
    }
    
    function logout(){
        $session = \Config\Services::session();
        $session->remove('userapp');
        $data['view']['title'] = "Merci";
        $data['page'] = "home";
        $data['code'] = 'home';
        $page = new PageModel();
        $page->affiche($data);
    }
}
