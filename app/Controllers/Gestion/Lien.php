<?php 
namespace App\Controllers\Gestion;
use \App\Controllers\BaseController;
use App\Models\PageModel;
use App\Models\LienModel;

//namespace App\Controllers;
//use \App\Controllers\Page;

class Lien extends BaseController
{
	//	public static $dbtable = "lien";
	public static $page = "lien";
	public static $table = 'lien';


	public function __construct()
	{
		$session = \Config\Services::session();
        $userapp = $session->userapp;
       if($userapp == false || $userapp  = NULL || empty($userapp ))
        {

            $data['msgerr'] = "";
            $page = new PageModel();
            $page->gestion($data,'login');
            exit;
        }
	}



	public function index(){
		$this->liste();

	 }
 
	 public function liste(){
		$data['view']['title'] = "Liste des liens";
		$data['table'] = self::$table;
        $data['page'] = self::$page;

		//$data['page'] = "lien";

		$strsql = "SELECT * FROM lien order by liordre";
		$db = db_connect();
        $query = $db->query($strsql);
       $data['t'] = $query->getResult();
 
		//$page = new Page();
       // $page->admin(self::$page.'-liste',$data);
		$page = new PageModel();
        $page->gestion($data,self::$page.'-liste');



	 }

	 /* ===== affichage formulaire creation ===== */
	 function ajout(){
		$data['view']['title'] = "Ajout lien";
		//	 $data['dbtable'] = self::$dbtable;
			 $data['page'] = self::$page;
			 $data['action'] = 'add';
			/* $data['r']['liurl'] = '';
			 $data['r']['lititre'] = '';
			 $data['r']['liordre'] = '';
			 $data['r']['liimage'] = '';
			 $data['r']['liicone'] = '';
			 $data['r']['livisible'] = '1';*/
			// echo view('lien-edit', $data);


			$r = new class{};
			$r->liurl = '';
			$r->lititre = '';
			$r->liordre = '0';
			$r->liimage = '';
			$r->liicone = '';
			$r->livisible = '1';
			$data['r'] = $r;

			$page = new PageModel();
			$page->gestion($data,self::$page.'-edit');
		//	 $page->admin(self::$page.'-edit',$data);
	


			/* $data['view']['title'] = "Ajout ".self::$table;
			 $data['action'] = 'add';
			 $data['page'] = self::$page;
	 
			 $r = new class{};
			 $r->code = '';
			 $r->titre = "";
	 
			 $r->textecourt = "";
			 $r->textelong = "";
			 $r->metatitle = "";
			 $r->metadescription = "";
			 $r->typepage = "";
			 $r->ordre = 0;
			 $r->visible = 1;
			 $r->datdeb = '2000-01-01';
			 $r->datfin = '2999-12-31';
	 
			 $data['r'] = $r;
	 
			 $param = new ParamModel();
			 $data['typepages']  = $param->getparams('typepage');
	  
			 $page = new PageModel();
			 $page->gestion($data,self::$page.'-edit');*/


	}


    function add(){
        $d = $this->addupd('a');
        //var_dump($d);
        $db = db_connect();

        $db->table(self::$table)->insert($d);
        $this->liste();
	}



	
	public function edit($id){
		$data['view']['title'] = "Ajout lien";
		$data['page'] = self::$page;
		$data['view']['title'] = "Modification";
		$data['action'] = 'upd';
		/*$strsql = "select * from lien where liid = $id";

		$db = db_connect();
		$query = $db->query($strsql);
		$data['r'] = $query->getRowArray();*/
		//echo view('lien-edit', $data);

		$lien = new LienModel();
        $data['r'] = $lien->get1lien($id);

		$page = new PageModel();
        $page->gestion($data,self::$page.'-edit');
		
	}
	public function upd() {
		$id = $this->request->getVar('liid');
	//	echo "mise à jour".$id;
		
		$wudate = date('Y-m-d');
		//$session = \Config\Services::session($config);

		$d['liurl'] = $this->request->getVar('liurl');
		$d['lititre'] = $this->request->getVar('lititre');
		$d['liordre'] = $this->request->getVar('liordre');
		$d['livisible'] = $this->request->getVar('livisible');
        $d = $this->addupd('u');
		$db = db_connect();
		$builder = $db->table(self::$table);
		$builder->where('liid', $id);
		$builder->update($d);
		$this->liste(); 
	}


	/* ===== suppression etape 1 ===== */
function sup($id){

    $data['action'] = 'del';
	$data['view']['title'] = "Suppression lien";
	$data['page'] = self::$page;
	$db = db_connect();
	$strsql = "select * from lien where liid = $id";
    $query = $db->query($strsql);
    $data['r'] = $query->getRowArray();
	$page = new Page();
	$page->admin(self::$page.'-edit',$data);
   
}

/* ===== suppression reel ===== */
function del(){
    $id = $this->request->getVar('liid');
    $db = db_connect();
    $builder = $db->table(self::$table);
    $builder->where('liid', $id);
    $builder->delete();
    $this->liste(); 
}
	function addupd($t){
		$wudate = date('Y-m-d');
		//$session = \Config\Services::session($config);
		$d['liurl'] = $this->request->getVar('liurl');
		$d['lititre'] = $this->request->getVar('lititre');
		$d['liordre'] = $this->request->getVar('liordre');
		$d['livisible'] = $this->request->getVar('livisible');
		$d['liimage'] = $this->request->getVar('liimage');
		$d['liicone'] = $this->request->getVar('liicone');
		  
	/*	$d['datupd'] = $wudate;
		$d['usrupd'] = $session->userapp;
		if ($t=="a"){
			$d['datcrt'] = $wudate;
			$d['usrcrt'] =  $session->userapp;
			}*/
		return $d;
		}
		public function crttab(){
			$db = db_connect();
			$strsql = "DROP TABLE `tasko`;";
			echo '<br/>'.$strsql;
			$db = db_connect();
		   $db->query($strsql);
		$strsql = "CREATE TABLE lien ( 
			`liid` INTEGER, 
			`liurl` TEXT, 
			`lititre` TEXT, 
			`liordre` INT,
			`livisible` TEXT, 
			`liimage` TEXT, 
			`liicone` TEXT,
			`lidatcrt`	TEXT,
			`liusrcrt`	TEXT,
			`lidatupd`	TEXT,
			`liusrupd`	TEXT,
			PRIMARY KEY(`liid`) )";
	
		$db->query($strsql);
		}
		public function updtab1(){
			// ajout du champ liordre
				$db = db_connect();
				$strsql = "ALTER TABLE lien  ADD liordre int";
				 echo '<br/>'.$strsql;
				$db = db_connect();
				   try
				{
					$db->query($strsql);
				}
				catch (\Exception $e)
				{
					echo "colonne liordre existe ";
				}

			// ajout du champ livisible
			   $strsql = "ALTER TABLE lien  ADD livisible TEXT";
			   echo '<br/>'.$strsql;
	
			  try
			  {
				  $db->query($strsql);
			  }
			  catch (\Exception $e)
			  {
				  echo "colonne livisible existe ";
			  }

			// ajout du champ liimage
			$strsql = "ALTER TABLE lien  ADD liimage TEXT";
			echo '<br/>'.$strsql;
  			  try {	 $db->query($strsql); }
			 catch (\Exception $e)	 { 	 echo "colonne liimagee existe ";	 }

			// ajout du champ liicone
			$strsql = "ALTER TABLE lien  ADD liicone TEXT";
			echo '<br/>'.$strsql;
  			  try {	 $db->query($strsql); }
			 catch (\Exception $e)	 { 	 echo "colonne liicone existe ";	 }



			}

	


}