<?php namespace App\Controllers\Gestion;
use \App\Controllers\BaseController;
use App\Models\MenuModel;
use App\Models\PageModel;


class Menu extends BaseController {
    public static  $page = 'menu';
    public static  $table = 'menu';

    public function __construct()
    {
        $session = \Config\Services::session();
        $userapp = $session->userapp;
        if($userapp == false || $userapp  = NULL || empty($userapp ))
        {

            $data['msgerr'] = "";
            $page = new PageModel();
            $page->gestion($data,'login');
            exit;
        }
    }
  

    public function index()
	{
        $this->liste('menu');
    
	}
    public function liste($menucode)
	{
        $data['view']['title'] = "Menu ".$menucode;
        $data['table'] = self::$table;
        $data['page'] = self::$page;
        $data['menucode'] = $menucode;
        $menu = new MenuModel();
        $data['t'] = $menu->getMenus($menucode);

        $page = new PageModel();
        $page->gestion($data,'menu-liste');
       // echo 'liste';
    }

    public function ajout($menucode)
    {
        $data['view']['title'] = "Paramètres ".$menucode;
        $data['action'] = 'add';
        $data['page'] = self::$page;
   
      //  echo $paty;
          // initialisaiton des champs du formulaire
        $r = new class{};
        $r->menucode = $menucode;
        $r->texte = '';
        $r->link = '';
        $r->ordre = '';
        $r->disabled = 0;
         $data['r'] = $r;

        $page = new PageModel();
        $page->gestion($data,self::$page.'-edit');
    }
    function add(){
        $menucode = $this->request->getVar('menucode');
        $d = $this->addupd('a');
        echo 'menucode '.$d['menucode'];
        $db = db_connect();
        $db->table(self::$table)->insert($d);
        $this->liste($menucode);
    }


    function edit($id){
        $data['page'] = self::$page;
        $data['view']['title'] = "Modification";
        $data['action'] = 'upd';

        $menu = new MenuModel();
        $data['r'] = $menu->get1menu($id);


        $page = new PageModel();
        $page->gestion($data,self::$page.'-edit');
    }

    public function upd() {
        $id = $this->request->getVar('id');
       $d = $this->addupd('u');

        $db = db_connect();
        $builder = $db->table(self::$table);
        $builder->where('id', $id);
        $builder->update($d);
        $menucode = $d['menucode'];
        $this->liste($menucode);
    }

    function addupd($t){
 
        $session = \Config\Services::session();
        $d['menucode'] = $this->request->getVar('menucode');
        $d['texte'] = $this->request->getVar('texte');
        $d['link'] = $this->request->getVar('link');
        $d['ordre'] = $this->request->getVar('ordre');
       $cb = $this->request->getVar('disabled') ;
        if (isset($cb))       
        {
            $d['disabled'] = 1;
            echo 'disabled null';
        }
        else
        {
            $d['disabled'] = 0;
            echo 'disabled pas null';
        }
        $d['usrmod'] = $session->userapp;
        $d['datmod'] = date('Y-m-d H:m');
        if ($t=="a"){
              $d['usrcrt'] = $session->userapp;
            }
        return $d;
        }

/* ===== suppression etape 1 ===== */
function sup($id){

    $data['page'] = self::$page;
    $data['view']['title'] = "Modification";
    $data['action'] = 'del';

    $param = new ParamModel();
    $data['r'] = $param->get1param($id);


    $page = new PageModel();
    $page->gestion($data,self::$page.'-edit');


}

    /* ===== suppression reel ===== */
    function del(){

        $id = $this->request->getVar('id');
        $paty = $this->request->getVar('paty');
        $db = db_connect();
        $builder = $db->table(self::$table);
        $builder->where('id', $id);
        $builder->delete();
        $this->liste($paty);

    }
    public function crttab()
    {
            $menu = new MenuModel();
            $menu->CreationTable();
    }


}