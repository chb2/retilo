<?php namespace App\Controllers\Gestion;
use \App\Controllers\BaseController;
use App\Models\ParamModel;
use App\Models\PageModel;
use App\Models\PageroModel;
use App\Models\ToolModel;
use CodeIgniter\Files\File;


class Pagero extends BaseController {

  public static  $table = 'pagero';
  public static  $page = 'pagero';
   
  public function __construct()
  {
      $session = \Config\Services::session();
      $userapp = $session->userapp;
      if($userapp == false || $userapp  = NULL || empty($userapp ))
      {
        $data['msgerr'] = "";
        $page = new PageModel();
        $page->gestion($data,'login');
        exit;
      }
  }


// liste 
    public function index()
	{
       // echo 'cms page';
        $this->liste();
	}

    public function liste($pageid = 0)
	{

        // faire une méthode ...
        //$data['view']['title'] = "Articles";
        $data['page'] = self::$page;
        $data['table'] = self::$table;
      if (isset($pageid) and $pageid == 0)
      {
        $data['pageid'] = 0;
        $strsql = "SELECT *  FROM ".self::$table."  ";
      }
      else
      {

        $data['pageid'] = $pageid;
        $strsql = "SELECT *  FROM ".self::$table."  where pageid = $pageid";
       //  echo  self::$page;



        }
        $strsql .= " order by id desc";
        $db = db_connect();
        $query = $db->query($strsql);
        $data['t'] = $query->getResult();
        $page = new PageModel();
        $page->gestion($data,'pagero-liste');

      }

    function test(){
        echo "test";
    }      
    function ajout($pageid){
       // echo "ajout";
        // $data['dbtable'] = 'ad_adresse';
       // $data['view']['title'] = "Ajout ".self::$table;
        $data['action'] = 'add';
        $data['page'] = self::$page;

        $r = new class{};
        $r->pageid = $pageid;
        $r->titre = "";
        $r->media = "";
        $r->textelong = "";
        $r->metatitle = "";
        $r->metadescription = "";
        $r->visible = 1;
        $r->datdeb = date('Y-m-d');
        $r->datfin = '2999-12-31';

        $data['r'] = $r;

     /*   $param = new ParamModel();
		$data['typepages']  = $param->getparams('typepage');*/
 
        $page = new PageModel();
        $page->gestion($data,self::$page.'-edit');
    }
    
    function add(){
        $d = $this->addupd('a');
        $idimg = date('Ymdhis');
        $media = $this->uplfile($idimg);
        if ($media != '0')    $d['media'] = $media;

        $pageid = $d['pageid'];
        $db = db_connect();
        $db->table(self::$table)->insert($d);
        $this->liste($pageid);
    }
    function edit($id){

        $data['page'] = self::$page;
        $data['view']['title'] = "Modification";
        $data['action'] = 'upd';

        $pagero = new PageroModel();
        $data['r'] =  $pagero->get1perid($id);


        $param = new ParamModel();
	    $data['typepages']  = $param->getparams('typepage');

        $page = new PageModel();
        $page->gestion($data,self::$page.'-edit');
    }
   
    function uplfile($id){
        // cette fonction télécharge une image dans p/media. On va ajouter le paramétrage plus tard 
        // on retourne l'url pour mettre à jour le url de l'image
        $validationRule = [
            'file_to_upload' => [
                'label' => 'Image File',
                'rules' => [
                  //  'uploaded[file_to_upload]',
                    'is_image[file_to_upload]',
                    'mime_in[file_to_upload,image/jpg,image/jpeg,image/gif,image/png,image/webp]',
                 //   'max_size[userfile,10000]',
                    //'max_dims[userfile,1024,768]',
                ],
            ],
        ];

        if (! $this->validateData([], $validationRule)) {
            $data = ['errors' => $this->validator->getErrors()];
            echo 'erreur ';
            exit;
           // return view('upload_form', $data);
        }

        $fileInfo = $this->request->getFile('file_to_upload');
        if ($fileInfo->getSize() > 0 )
        {
            if ($fileInfo->isValid() && !$fileInfo->hasMoved()  ){
                $ext = $fileInfo->guessExtension();
                //$newname = $fileInfo->getName();
                $newname = 'imgpagero-'.$id.'.'.$ext;
                // recherche du chemin racine
                $param = new ParamModel();
                //$r = $param->get1paramPerCode('site','datradikloko');
                //$rootpath = $r->zona;
                $rootpath = FCPATH.'media';  
               // echo $rootpath;


                //$r = $param->get1paramPerCode('site','datradikurl');
                //$rooturl = $r->zona;
                $rooturl = base_url('media');  


             //   $r = $param->get1paramPerCode('site','datlokoslosilo');
             //   $cle = $r->zona;
            
            
                //$path = ROOTPATH.'p/images';
               $path = $rootpath;
                $url = $rooturl.'/'.$newname;
                $fileInfo->move($path,$newname,true);
                return $url;
  
            }
        }
        else
        {

            return '0';
        }
    }

    public function upd() {
        // si changement image, il faudra supprimer l'ancienne
        
        $id = $this->request->getVar('id');
        $pageid = $this->request->getVar('pageid');
        $d = $this->addupd('u');
        $media = $this->uplfile($id);
        if ($media != '0')    $d['media'] = $media;

        $db = db_connect();
        $builder = $db->table(self::$table);
        $builder->where('id', $id);
        $builder->update($d);
        $this->liste($pageid);
    }
       
    function addupd($t){
        $wudate = date('Y-m-d');
        $session = \Config\Services::session();
        $d['pageid'] = $this->request->getVar('pageid');
        $d['titre'] = $this->request->getVar('titre');
        $d['media'] = $this->request->getVar('media');
        $d['textelong'] = $this->request->getVar('textelong');
        $d['metatitle'] = $this->request->getVar('metatitle');
        $d['metadescription'] = $this->request->getVar('metadescription');
        $d['visible'] = $this->request->getVar('visible');
        $d['datdeb'] = $this->request->getVar('datdeb');
        $d['datfin'] = $this->request->getVar('datfin');
          
        $d['datmod'] = $wudate;
        $d['usrmod'] = $session->userapp;
        if ($t=="a"){
            $d['datcrt'] = $wudate;
            $d['usrcrt'] =  $session->userapp;
            }
        return $d;
        }

        /* ===== suppression etape 1 ===== */
function sup($id){

    $data['page'] = self::$page;
    $data['view']['title'] = "Suppression";
    $data['action'] = 'del';

    $pagero = new PageroModel();
    $data['r'] =  $pagero->get1perid($id);

    $page = new PageModel();
    $page->gestion($data,self::$page.'-edit');




}

/* ===== suppression reel ===== */
 function del(){

    $id = $this->request->getVar('id');
    $pageid = $this->request->getVar('pageid');
   // echo 'suppression '.$id;
   // echo 'page id '.  $pageid;
    $db = db_connect();
    $builder = $db->table(self::$table);
    $builder->where('id', $id);
    $builder->delete();
 
   $this->liste($pageid);

}
/* =====  ===== */
    public function crttab(){


        $pagero = new PageroModel();
        $pagero ->CreationTable(); 

    }

	public function updtab1(){
        $this->addfield('typepage','TEXT');
    }
    public function addfield($field, $type){
        $db = db_connect();
        $strsql = "ALTER TABLE ".self::$table."  ADD $field $type";
        echo '<br/>'.$strsql;
        try {	 $db->query($strsql); }
         catch (\Exception $e)	 { 	 echo "<br/>Column $field already exists ";	 }

    }
    
    public function updtab2(){
        $tool = new ToolModel();
        $tool->dropfield(self::$table,'image1');
        $tool->addfield(self::$table,'media','TEXT');
    }

}
