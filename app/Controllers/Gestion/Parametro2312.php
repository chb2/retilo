<?php namespace App\Controllers\Gestion;
use \App\Controllers\BaseController;
use App\Models\ParamModel;
use App\Models\PageModel;


class Parametro extends BaseController {
    public static  $page = 'parametro';
    public static  $table = 'param';

    public function __construct()
    {
        $session = \Config\Services::session();
        $userapp = $session->userapp;
        if($userapp == false || $userapp  = NULL || empty($userapp ))
        {

            $data['msgerr'] = "";
            $page = new PageModel();
            $page->gestion($data,'login');
            exit;
        }
    }
  

    public function index()
	{

        $data['page'] = self::$page;
        $data['view']['title'] = "Paramètres ";



        $param = new ParamModel();
        $p1 = $param->get1paramPerCode('site','defaultpage');
        $site_defaultpage = $p1->zona;

        $data['site_defaultpage'] = $site_defaultpage;

        $p1 = $param->get1paramPerCode('site','komputo');
        $site_komputo = $p1->zona;
        $data['site_komputo'] = $site_komputo;

       // $data['p1']['paco'];

       $page = new PageModel();
        $page->gestion($data,'parametrov');
    
	}
    public function liste($paty)
	{
        $data['view']['title'] = "Paramètres ".$paty;
        $data['table'] = self::$table;
        $data['page'] = self::$page;
        $data['paty'] = $paty;

        $param = new ParamModel();
        $data['t'] = $param->getParams($paty);

        $page = new PageModel();
        $page->gestion($data,'param-liste');
    }

    function edityco($tyco)
    {

        $tycoar = explode("_", $tyco);
        $paty = $tycoar[0];
        $paco = $tycoar[1];
        $param = new ParamModel();
        $data['r']= $param->get1paramPerCode($paty,$paco);

        $page = new PageModel();
        $data['view']['title'] = "Paramètre ".$paty.' '.$paco;

        $page->gestion($data,'parametro-edit');

    }

    public function updtyco() {
        $paty = $this->request->getVar('paty');
        $paco = $this->request->getVar('paco');
        $db = db_connect();
        $builder = $db->table(self::$table);
        $builder->set('zona', $this->request->getVar('zona'));
        $builder->where('paty', $this->request->getVar('paty'));
        $builder->where('paco', $this->request->getVar('paco'));
        $builder->update();
   
        $this->index();
    }

  /*  function addupd($t){
        $d['paty'] = $this->request->getVar('paty');
        $d['paco'] = $this->request->getVar('paco');
        $d['zona'] = $this->request->getVar('zona');
    }*/

/*
    public function ajout($paty)
    {
        $data['view']['title'] = "Paramètres ".$paty;
          $data['action'] = 'add';
          $data['page'] = self::$page;
   
      //  echo $paty;
          // initialisaiton des champs du formulaire
          $r = new class{};
          $r->paty = $paty;
          $r->paco = '';
          $r->libelle = '';
          $r->zona = '';
          $r->disabled = 0;
         $data['r'] = $r;

          $page = new PageModel();
          $page->gestion($data,self::$page.'-edit');
    }
    function add(){
        $paty = $this->request->getVar('paty');
        //echo 'add'.$paty;
        $d = $this->addupd('a');
        $db = db_connect();
        $db->table(self::$table)->insert($d);
        $this->liste($paty);
    }



    public function upd() {
        $id = $this->request->getVar('id');
      //  echo $id;
        $d = $this->addupd('u');
        $db = db_connect();
        $builder = $db->table(self::$table);
        $builder->where('id', $id);
        $builder->update($d);
        $paty = $d['paty'];
        $this->liste($paty);
    }

    function addupd($t){
 
        $session = \Config\Services::session();
        $d['paty'] = $this->request->getVar('paty');
        $d['paco'] = $this->request->getVar('paco');
        $d['libelle'] = $this->request->getVar('libelle');
        $d['zona'] = $this->request->getVar('zona');
        $d['disabled'] = $this->request->getVar('disabled');
 
 
        $d['usrmod'] = $session->userapp;
        $d['datmod'] = date('Y-m-d H:m');
        if ($t=="a"){
              $d['usrcrt'] = $session->userapp;
            }
        return $d;
        }

/* ===== suppression etape 1 ===== */
/*
function sup($id){

    $data['page'] = self::$page;
    $data['view']['title'] = "Modification";
    $data['action'] = 'del';

    $param = new ParamModel();
    $data['r'] = $param->get1param($id);


    $page = new PageModel();
    $page->gestion($data,self::$page.'-edit');


}
*/
    /* ===== suppression reel ===== */
 /*   function del(){

        $id = $this->request->getVar('id');
        $paty = $this->request->getVar('paty');
        $db = db_connect();
        $builder = $db->table(self::$table);
        $builder->where('id', $id);
        $builder->delete();
        $this->liste($paty);

    }
    public function crttab()
    {
            $param = new ParamModel();
            $param->CreationTable();
    }

    public function updtab(){
        $this->addfield('disabled','INTEGER',0);
    }

    public function addfield($field, $type,$default){
        $db = db_connect();
        $strsql = "ALTER TABLE param  ADD $field $type default $default";
        echo '<br/>'.$strsql;
        try {	 $db->query($strsql); }
        catch (\Exception $e)	 { 	 echo "<br/>Column $field already exists ";	 }

    }
*/
}