<?php 
 namespace App\Controllers\Gestion;
 use \App\Controllers\BaseController;
 use App\Models\PageModel;
 use App\Models\ParramModel;

 class Home extends BaseController
{

	public function __construct()
    {
       $session = \Config\Services::session();
        $userapp = $session->userapp;
       if($userapp == false || $userapp  = NULL || empty($userapp ))
        {

            $data['msgerr'] = "";
            $page = new PageModel();
            $page->gestion($data,'login');
            exit;
        }
    }

    public function index()
    {
         
         $data['view']['title'] = "Gestion";
         $page = new PageModel();
         $page->gestion($data,'home');
 
    }
 


}
