<?php namespace App\Controllers\Gestion;
use \App\Controllers\BaseController;
use App\Models\ParamModel;
use App\Models\LogModel;
use App\Models\PageModel;


class Log extends BaseController {
    public static  $page = 'log';
    public static  $table = 'log';

    public function __construct()
    {
        $session = \Config\Services::session();
        $userapp = $session->userapp;
        if($userapp == false || $userapp  = NULL || empty($userapp ))
        {

            $data['msgerr'] = "";
            $page = new PageModel();
            $page->gestion($data,'login');
            exit;
        }
    }
  
    public function index()
	{
        $this->liste();
       
    
	}

    public function liste()
	{
        
        $data['view']['title'] = "Log";
        $data['table'] = self::$table;
        $data['page'] = self::$page;
        $strsql = "select * from ".self::$table." order by id desc limit 300";
        $db = db_connect();
        $query = $db->query($strsql);
       $data['t'] = $query->getResult();

       //$uzanto = new UzantoModel();
       //$data['uzantoj'] = $uzanto->getuzantoj();

        $page = new PageModel();
        $page->gestion($data,self::$page.'-liste');
    }

    public function crttab()
    {
        $log = new LogModel();
        $log->CreationTable();
    }

}


