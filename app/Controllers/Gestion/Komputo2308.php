<?php namespace App\Controllers\Gestion;
use \App\Controllers\BaseController;
use App\Models\KomputoModel;
use App\Models\PageModel;
/*

 Komputo : compteur
*/

class Komputo extends BaseController {
    public static  $page = 'komputo';
    public static  $table = 'komputo';

    public function __construct()
    {
        $session = \Config\Services::session();
        $userapp = $session->userapp;
        if($userapp == false || $userapp  = NULL || empty($userapp ))
        {
          $data['msgerr'] = "";
          $page = new PageModel();
          $page->gestion($data,'login');
          exit;
        }
    }
    public function index()
	  {
        $this->liste();
       
    }
 
  public function liste()
	{
        
        $data['view']['title'] = "Statistiques affichage";
        $data['table'] = self::$table;
        $data['page'] = self::$page;
        // a remplacer par une méthode getuzantoj('id')
        $strsql = "select * from komputo order by url, periodatipo, periodavaloro,id";
        $db = db_connect();
        $query = $db->query($strsql);
        $data['t'] = $query->getResult();



        $page = new PageModel();
        $page->gestion($data,'komputo-liste');

    }

/* ===== suppression etape 1 ===== */
function sup($id){

  $data['page'] = self::$page;
  $data['view']['title'] = "Suppression";
  $data['action'] = 'del';

  $komputo = new KomputoModel();
  $data['r'] = $komputo->get1perid($id);


  $page = new PageModel();
  $page->gestion($data,self::$page.'-edit');


}

  /* ===== suppression reel ===== */
  function del(){



$id = $this->request->getVar('id');
 
     $db = db_connect();
      $builder = $db->table(self::$table);
      $builder->where('id', $id);
      $builder->delete();
      $this->liste();

  }
    public function crttab()
	{
        #Création de la table. A l'avenir il faudra faire une création des table avec un autre controleur 

        #$this->liste();
        $komputo = new KomputoModel();
        $komputo->kreitablo();
	}
  /*  public function updtab1()
	{
        #Création de la table. A l'avenir il faudra faire une création des table avec un autre controleur 

        #$this->liste();
        $uzanto = new UzantoModel();
        $uzanto->addremarquefield();
	}*/

}