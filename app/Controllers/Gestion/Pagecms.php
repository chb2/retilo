<?php namespace App\Controllers\Gestion;
use \App\Controllers\BaseController;
use App\Models\ParamModel;
use App\Models\PageModel;
use App\Models\PagecmsModel;


class Pagecms extends BaseController {

  public static  $table = 'page';
  public static  $page = 'pagecms';
   
  public function __construct()
  {
      $session = \Config\Services::session();
      $userapp = $session->userapp;
      if($userapp == false || $userapp  = NULL || empty($userapp ))
      {
        $data['msgerr'] = "";
        $page = new PageModel();
        $page->gestion($data,'login');
        exit;
      }
  }


// liste 
    public function index()
	{
       // echo 'cms page';
        $this->liste();
	}

    public function liste()
	{
       
        $data['view']['title'] = "Pages";
        $data['page'] = self::$page;
        $data['table'] = self::$table;
        $strsql = "SELECT *  FROM ".self::$table." ";
        // echo  self::$page;
        $db = db_connect();
        $query = $db->query($strsql);
        $data['t'] = $query->getResult();

        $page = new PageModel();
        $page->gestion($data,'pagecms-liste');
      }

    function test(){
        echo "test";
    }      
    function ajout(){
       // echo "ajout";
        // $data['dbtable'] = 'ad_adresse';
        $data['view']['title'] = "Ajout ".self::$table;
        $data['action'] = 'add';
        $data['page'] = self::$page;

        $r = new class{};
        $r->code = '';
        $r->titre = "";

        $r->textecourt = "";
        $r->textelong = "";
        $r->metatitle = "";
        $r->metadescription = "";
        $r->typepage = "";
        $r->ordre = 0;
        $r->visible = 1;
        $r->datdeb = '2000-01-01';
        $r->datfin = '2999-12-31';

        $data['r'] = $r;

        $param = new ParamModel();
		$data['typepages']  = $param->getparams('typepage');
 
        $page = new PageModel();
        $page->gestion($data,self::$page.'-edit');
    }
    
    function add(){
  
           $d = $this->addupd('a');
           //var_dump($d);
           $db = db_connect();
           $db->table(self::$table)->insert($d);
           $this->liste();
       }
    function edit($id){

        $data['page'] = self::$page;
        $data['view']['title'] = "Modification";
        $data['action'] = 'upd';
        $strsql = "select * from ".self::$table." where id = $id";
        
       // echo $strsql;
        //$db = db_connect();
        //$query = $db->query($strsql);
        //$data['r'] = $query->getRowArray();


        $pagecms = new PagecmsModel();
        $data['r'] =  $pagecms->get1perid($id);


        $param = new ParamModel();
		$data['typepages']  = $param->getparams('typepage');

        $page = new PageModel();
        $page->gestion($data,self::$page.'-edit');
    }
   
    public function upd() {
        $id = $this->request->getVar('id');
        $d = $this->addupd('u');
        $db = db_connect();
        $builder = $db->table(self::$table);
        $builder->where('id', $id);
        $builder->update($d);
        $this->liste();
    }
       
    function addupd($t){
        $wudate = date('Y-m-d');
        $session = \Config\Services::session();
        $d['code'] = $this->request->getVar('code');
        $d['titre'] = $this->request->getVar('titre');
        $d['textecourt'] = $this->request->getVar('textecourt');
        $d['textelong'] = $this->request->getVar('textelong');
        $d['metatitle'] = $this->request->getVar('metatitle');
        $d['metadescription'] = $this->request->getVar('metadescription');
        $d['typepage'] = $this->request->getVar('typepage');
        $d['ordre'] = $this->request->getVar('ordre');
        $d['visible'] = $this->request->getVar('visible');
        $d['datdeb'] = $this->request->getVar('datdeb');
        $d['datfin'] = $this->request->getVar('datfin');
          
        $d['datmod'] = $wudate;
        $d['usrmod'] = $session->userapp;
        if ($t=="a"){
            $d['datcrt'] = $wudate;
            $d['usrcrt'] =  $session->userapp;
            }
        return $d;
        }

        /* ===== suppression etape 1 ===== */
function sup($id){

    $data['page'] = self::$page;
    $data['view']['title'] = "Suppression";
    $data['action'] = 'del';

    $pagecms = new PagecmsModel();
    $data['r'] =  $pagecms->get1perid($id);

    $param = new ParamModel();
    $data['typepages']  = $param->getparams('typepage');


    $page = new PageModel();
    $page->gestion($data,self::$page.'-edit');


}

/* ===== suppression reel ===== */
 function del(){

    $id = $this->request->getVar('id');
    $paty = $this->request->getVar('paty');
    $db = db_connect();
    $builder = $db->table(self::$table);
    $builder->where('id', $id);
    $builder->delete();
    $this->liste($paty);

}
/* =====  ===== */
    public function crttab(){


        $pagecms = new PagecmsModel();
        $pagecms ->CreationTable(); 

    }

	public function updtab1(){
        $this->addfield('typepage','TEXT');
    }
    public function addfield($field, $type){
        $db = db_connect();
        $strsql = "ALTER TABLE ".self::$table."  ADD $field $type";
        echo '<br/>'.$strsql;
        try {	 $db->query($strsql); }
         catch (\Exception $e)	 { 	 echo "<br/>Column $field already exists ";	 }

    }
    

}
