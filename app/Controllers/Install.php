<?php 

namespace App\Controllers;
use \App\Controllers\BaseController;
use App\Models\InstallModel;
use App\Models\ParamModel;
use App\Models\UzantoModel;
use App\Models\PageModel;
use App\Models\LogModel;
use App\Models\MenuModel;


class Install extends BaseController
{


    public function __construct()
    {
       /* $install = new InstallModel();
        $ok = $install->installchk();
        if ($ok == 1)
        {
            $this->partie1controle();
            exit;
        }
        */
    }
  
    public function test()
	{
         echo 'test';
	}
    public function index()
	{

        //echo 'install';
        $this->partie1controle();   

	}

    public function crttab()
	{
       // echo 'installation création table';
        $log = new LogModel();
        $log->CreationTable();

        $menu = new MenuModel();
        $menu->CreationTable();


        $param = new ParamModel();
        $param->CreationTable();


        $uzanto = new UzantoModel();
        $uzanto->CreationTable();

    }
public function partie1controle()
	{
        $install = new InstallModel();
        $crtadmin = $install->getuzantoAdmin();
        if ($crtadmin == 1 )
        {
          $data['page'] = "install";
          $data['action'] = "installok";
          $page = new PageModel();
          $page->page($data);
          exit;
        }
        else
        {
            $this->partie2saisieinfo();  

        }

    }

    public function partie2saisieinfo()
	{
      //  echo 'partie2';
      //  exit;
        $data['view']['title'] = "Installation";
        $data['page'] = "install";
        $data['action'] = "install-crtadmin-edit";
        $data['msg'] = "Création d'un utilisateur administrateur";
         $page = new PageModel();
         $page->page($data);
    }

    public function partie3installation()
	{

        $log = new LogModel();
        $log->CreationTable();

        $param = new ParamModel();
        $param->CreationTable();


        $uzanto = new UzantoModel();
        $uzanto->CreationTable();

        // la création uzanto devra se faire dans le modèle
        $u = $this->request->getVar('u');
        $d['code'] = $this->request->getVar('u');
        $d['pasvor'] = md5($this->request->getVar('p'));
        $d['usrcrt'] = $this->request->getVar('u');
        $d['usrmod'] = $this->request->getVar('u');
        $d['grpaut'] = 'A';
        $db = db_connect();
        $db->table('uzanto')->insert($d);


         $data['view']['title'] = "Installation";
         $data['page'] = "install";
         $data['action'] = "installok";
         $data['msg'] = "L'installation a bien été faite";
         $data['msg'] .= "<br/>Utilisation.".$u." créé en tant qu'admin";
          $page = new PageModel();
          $page->page($data);
    }
    
    
}
