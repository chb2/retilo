<?php namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\PagecmsModel;
use App\Models\PageModel;

    class Page extends Controller {

    public function index()
    {
        
        return view('welcome_message');
    }

    public function p($p)
    {
        $cmspage = new PagecmsModel();
		$r = $cmspage->get1percode($p);

       
         $data['page'] = "page";
         $data['code'] = $r->code;
         $data['view']['title'] = $r->titre;
         $data['meta']['title'] = $r->titre;
         $data['meta']['description'] =  $r->textecourt;

         $data['r'] = $r;
         $page = new PageModel();
         $page->affiche($data);

    }
/*
    public function showme($page = 'home',$data)
    {
        //echo "shome";
        if ( ! is_file(APPPATH.'/Views/'.$page.'.php'))
        {
            // Whoops, we don't have a page for that!
            throw new \CodeIgniter\Exceptions\PageNotFoundException($page);
        }

        $data['title'] = ucfirst($page); // Capitalize the first letter
        $session = \Config\Services::session();
        $data['userapp'] = $session->userapp;
        $data['useradmin'] = $session->useradmin;

        echo view('template/header', $data);
        echo view($page.'.php', $data);
        echo view('template/footer', $data);
    }

    // template pour l'affichage des pages pour l'admin
    public function admin($page = 'home',$data)
    {
        if ( ! is_file(APPPATH.'/Views/admin/'.$page.'.php'))
        {
            // Whoops, we don't have a page for that!
            throw new \CodeIgniter\Exceptions\PageNotFoundException($page);
        }

        $data['title'] = ucfirst($page); // Capitalize the first letter
        $session = \Config\Services::session();
        $data['userapp'] = $session->userapp;
        $data['useradmin'] = $session->useradmin;

        echo view('admin/template/header', $data);
        echo view('admin/'.$page.'.php', $data);
        echo view('admin/template/footer', $data);
    }

    public function prestataire($page = 'home',$data)
    {
        //echo $page;
        $repview="prestataire";
        if (isset($data['repview'])){$repview=$data['repview'];}
       
        if ( ! is_file(APPPATH.'/Views/'.$repview.'/'.$page.'.php'))
        {
            // Whoops, we don't have a page for that!
            throw new \CodeIgniter\Exceptions\PageNotFoundException($page);
        }

        $data['title'] = ucfirst($page); // Capitalize the first letter
        $session = \Config\Services::session();
        $data['userapp'] = $session->userapp;
        $data['useradmin'] = $session->useradmin;

        echo view('template/header', $data);
        echo view($repview.'/'.$page.'.php', $data);
        echo view('template/footer', $data);
    }


    public function projet($page = 'home',$data)
    {
        //echo $page;
        if ( ! is_file(APPPATH.'/Views/projet/'.$page.'.php'))
        {
            // Whoops, we don't have a page for that!
            throw new \CodeIgniter\Exceptions\PageNotFoundException($page);
        }

        $data['title'] = ucfirst($page); // Capitalize the first letter
        $session = \Config\Services::session();
        $data['userapp'] = $session->userapp;
        $data['useradmin'] = $session->useradmin;

        echo view('template/header', $data);
        echo view('projet/'.$page.'.php', $data);
        echo view('template/footer', $data);
    }


    // template pour l'affichage des pages du site
    public function page($page = 'home',$data)
    {
        if ( ! is_file(APPPATH.'/Views/'.$page.'.php'))
        {
            // Whoops, we don't have a page for that!
            throw new \CodeIgniter\Exceptions\PageNotFoundException($page);
        }

        $data['title'] = ucfirst($page); // Capitalize the first letter
        $session = \Config\Services::session();
        $data['userapp'] = $session->userapp;
        $data['useradmin'] = $session->useradmin;

        echo view('template/header', $data);
        echo view($page.'.php', $data);
        echo view('template/footer', $data);
    }**/
}
