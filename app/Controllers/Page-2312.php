<?php namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\PagecmsModel;
use App\Models\PageModel;
use App\Models\LienModel;
use App\Models\PageroModel;

    class Page extends Controller {

    public function index()
    {
        
        return view('welcome_message');
    }


    public function pagerojpost($p)
    {
        echo "pagerojposte";
    }
    public function pagerojante($p)
    {
        echo "pagerojante";
    }

    public function p($p)
    {


        // il faudra protéger l'existance du code si la page n'existe pas
        $cmspage = new PagecmsModel();
	    $r = $cmspage->get1percode($p);
        if (!isset($r)) {

            // Si le code n'est pas trouvé afficher une page 404
            $data['page'] = '404';
            $data['code'] = '404';
            $page = new PageModel();
            $page->affiche($data);
            exit;
        }

        $data['page'] = $r->code;
        $data['pageid'] = $r->id;

        if (!isset($r->typepage)) {
        $type = 'a';
        }
        else
        {
            $type = $r->typepage;
        }

        // commun pour tous les type
        $data['code'] = $r->code;


        if ($type =='')
        {
         
        $data['page'] = 'page';
        //$data['code'] = $r->code;
        $data['view']['title'] = $r->titre;
        $data['meta']['title'] = $r->titre;
        $data['meta']['description'] =  $r->textecourt;
        $data['r'] = $r;
        $page = new PageModel();
        $page->affiche($data);

        }

        if ($type =='link')
        {
            $data['page'] = "link";

            $lien = new LienModel();
            $l = $lien->getliens();
            $data['l'] = $l;
            // ce paramétre devra être dans la page type lien pour dire si on affiche ou non le menu
            $data['menu'] = '0';
            $data['pied'] = '0';
            $data['view']['title'] = $r->titre;
            $data['meta']['title'] = $r->titre;
            $data['meta']['description'] =  $r->textecourt;
            $data['r'] = $r;
            $page = new PageModel();
            $page->page($data);
    

        }
        if ($type =='blog')
        {

            $data['page'] = "blog";
            $pageid =  $data['pageid'];

            $pagero = new PageroModel();

            $pgrj = $pagero->getpageroj($pageid);
            $data['number_pageroj'] = count($pgrj);
            /*
            if ($this->request->getVar('pnb') !== null)
            {
               
            }
            else
            {
                $pnb = 1;
            }
*/

            $pnb = $this->request->getVar('pnb');
            if ($pnb > 1 )
            {
                $data['page_number'] = $pnb;
            }
            else
            {
                $data['page_number'] = 1;
            }

        
            $data['number_per_page'] = 5; 
            $date_poste =  date('Y-m-d');
            $id_poste = 1;
            $nb = 2;

            //$prs = $pagero->getpageroj_poste($date_poste,$id_poste,$nb);
           // $data['prs'] =   $prs;
            $data['prs'] =   $pgrj ;

            $data['view']['title'] = $r->titre;
            $data['meta']['title'] = $r->titre;
            $data['meta']['description'] =  $r->textecourt;
            $data['r'] = $r;

            $page = new PageModel();
            $page->page($data);

        }
      


    }

}
