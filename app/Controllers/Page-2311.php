<?php namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\PagecmsModel;
use App\Models\PageModel;
use App\Models\LienModel;
use App\Models\PageroModel;

    class Page extends Controller {

    public function index()
    {
        
        return view('welcome_message');
    }

    public function p($p)
    {
    
        // il faudra protéger l'existance du code si la page n'existe pas
        $cmspage = new PagecmsModel();
	    $r = $cmspage->get1percode($p);
        if (isset($r)) {
         // echo "trouvé";
        }
        else
        {
            // Si le code n'est pas trouvé afficher une page 404
            $data['page'] = '404';
            $data['code'] = '404';
            $page = new PageModel();
            $page->affiche($data);
            exit;
        }



        $data['page'] = $r->code;

 


        if (!isset($r->typepage)) {
        $type = '';
        }
        else
        {
            $type = $r->typepage;
        }
      //  echo $r->typepage;


        // commun pour tous les type
        $data['code'] = $r->code;
        if ($type =='')
        {
         
        $data['page'] = 'page';
        //$data['code'] = $r->code;
        $data['view']['title'] = $r->titre;
        $data['meta']['title'] = $r->titre;
        $data['meta']['description'] =  $r->textecourt;
        $data['r'] = $r;
        $page = new PageModel();
        $page->affiche($data);

        }
    //    echo $type;
    //    echo '<br/> data page'.$data['page'];
    //    echo '<br/> pageid'.$r->id;


   //    exit;
        if ($type =='link')
        {
            $data['page'] = "link";

            $lien = new LienModel();
            $l = $lien->getliens();
            $data['l'] = $l;
            // ce paramétre devra être dans la page type lien pour dire si on affiche ou non le menu
            $data['menu'] = '0';
            $data['pied'] = '0';
            $data['view']['title'] = $r->titre;
            $data['meta']['title'] = $r->titre;
            $data['meta']['description'] =  $r->textecourt;
            $data['r'] = $r;
            $page = new PageModel();
            $page->page($data);
    

        }

        if ($type =='blog')
        {

            $data['page'] = "blog";

            $pagero = new PageroModel();
            $prs = $pagero->getpageroj($r->id);
            $data['prs'] =   $prs;
            $data['view']['title'] = $r->titre;
            $data['meta']['title'] = $r->titre;
            $data['meta']['description'] =  $r->textecourt;
            $data['r'] = $r;
            $page = new PageModel();
            $page->page($data);

        }
      


    }

}
