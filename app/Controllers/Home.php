<?php

namespace App\Controllers;
use App\Models\PageModel;
use \App\Controllers\Page;
use App\Models\PagecmsModel;
use App\Models\ParamModel;


class Home extends BaseController
{

   public function __construct()
   {
       helper('url');
   }

    public function index()
    {
       
       $data['view']['title'] = "Accueil";



       // chercher la page par défaut
       $param = new ParamModel();
       $p1 = $param->get1paramPerCode('site','defaultpage');

      /* echo 'p1'.$p1->zona;
       exit;*/

       if (is_null($p1))
       {
          $site_defaultpage = '';
       }
       else
       {
          $site_defaultpage = $p1->zona;
       }


       $p =  $site_defaultpage ;

     //  $load->helper('url');
      header("Location:".base_url('index.php/page/p/'.$p));
      //redirect('page/p/home', 'refresh');
       exit;



       $pagecontroller = new Page();
       $pagecontroller->p($p);
       exit;


       $cmspage = new PagecmsModel();
       $r = $cmspage->get1percode($p);

        
          $data['page'] = "page";
          // code de la page
          $data['code'] = $r->code;
          $data['codepage'] = $r->code;
          $data['view']['title'] = $r->titre;
          $data['meta']['title'] = $r->titre;
          $data['meta']['description'] =  $r->textecourt;
 
          $data['r'] = $r;
          $page = new PageModel();
          $page->affiche($data);
 

    }
    public function langen()
    {
             $session = \Config\Services::session();
             $session->lang = 'en';
             $this->index();
    }
           public function langfr()
    {
             $session = \Config\Services::session();
             $session->lang = 'fr';
             $this->index();
    }
}
