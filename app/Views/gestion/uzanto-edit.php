<div class="container">
<h1 class="titrepage"><?= lang('Text.uzantotitle') ?></h1>
<?php


$useradmin='A';

helper('form');

if ($action == "add"){ 
    echo form_open_multipart('gestion/'.$page.'/add'); 
    $txtbouton = lang('Text.add');
    $classbouton = "class='btn btn-primary'";
}
if ($action == "upd"){ 
    echo form_open_multipart('gestion/'.$page.'/upd'); 
    $txtbouton = lang('Text.update');
    $classbouton = "class='btn btn-primary'";
}
if ($action == "del"){
     echo form_open_multipart('gestion/'.$page.'/del'); 
     $txtbouton = lang('Text.delete'); 
     $classbouton = "class='btn btn-danger'";}

?>
<?php
  if (!empty($erreurs)) {
    echo "<br/>";
    ?>
  <div class="alert alert-danger" role="alert">
 <p>Attention il y a au moins une erreur</p>
    <?php foreach ($erreurs as $erreur): ?>
        <li><?php echo $erreur; ?></li>
        <?php endforeach ?>
  </div>
  <?php } ?>


    <div class="form-group row">
        <span class="col-sm-2 col-form-label" ><?= lang('Text.userid') ?></span>
        <div class="col-10">
        <?php

            $data = array(
                        'name'        => 'code',
                        'type'        => 'text',
                        'value'       =>  $r->code,
                        'style'       => 'width: 100%'
                        );
            echo form_input($data);
            ?>
        </div>
    </div>


    <div class="form-group row">
        <span class="col-sm-2 col-form-label" ><?= lang('Text.username') ?></span>
        <div class="col-10">
        <?php

            $data = array(
                        'name'        => 'libelle',
                        'type'        => 'text',
                        'value'       =>  $r->libelle,
                        'style'       => 'width: 100%'
                        );
            echo form_input($data);
            ?>
        </div>
    </div>

    <div class="form-group row">
        <span class="col-sm-2 col-form-label" ><?= lang('Text.email') ?></span>
        <div class="col-4">
        <?php
            $data = array(
                        'name'        => 'email',
                        'type'        => 'text',
                        'value'       =>  $r->email,
                        'style'       => 'width: 100%'
                        );
            echo form_input($data);
            ?>
        </div>
        <div class="col-4">
            <?php
            echo $r->datvalemail;
            ?>
        </div>
    </div>
 
    <div class="form-group row">
        <span class="col-sm-2 col-form-label" ><?= lang('Text.password') ?></span>
        <div class="col-10">
        <?php

            $data = array(
                        'name'        => 'pasvor',
                        'type'        => 'text',
                        'value'       => ' ',
                        'style'       => 'width: 100%'
                        );
            echo form_password($data);
            ?>
        </div>
    </div>
 
    <div class="form-group row">
        <span class="col-sm-2 col-form-label" ><?= lang('Text.a_admin') ?></span>
        <div class="col-10">
        <?php

            $data = array(
                        'name'        => 'grpaut',
                        'type'        => 'text',
                        'value'       =>  $r->grpaut,
                        'style'       => 'width: 100%'
                        );
            echo form_input($data);
            ?>
        </div>
    </div>

    <div class="form-group row">
        <span class="col-sm-2 col-form-label" ><?= lang('Text.remark') ?></span>
        <div class="col-10">
        <?php
            $data = array(
                        'name'        => 'remarque',
                        'type'        => 'text',
                        'value'       =>  $r->remarque,
                        'style'       => 'width: 100%'
                        );
            echo form_textarea($data);
            ?>
        </div>
    </div>

    <?php  if ($action <> "add"){ ?>

<div class="form-group row">
    <span class="col-sm-2 text-secondary"><?= lang('Text.creation') ?></span>
    <span class="col-sm-2 text-secondary"><?php echo $r->datcrt;?> </span>
    <span class="col-sm-2 text-secondary"> <?php echo $r->usrcrt;?> </span>
</div>
<div class="row">
    <span class="col-sm-2 text-secondary"><?= lang('Text.update') ?></span>
    <span class="col-sm-2 text-secondary"><?php echo $r->datmod;?></span>
    <span class="col-sm-2 text-secondary"> <?php echo $r->usrmod;?> </span>
</div>
    <?php } ?>

 
 
<div class="form-group row">
    <div class="col-sm-2">
    <?php

    if ($action <> "add"){
    echo form_hidden('id',$r->id);}


    echo form_submit('submit',$txtbouton, $classbouton);

    echo form_close();
    ?>
    </div>
</div>
</div>