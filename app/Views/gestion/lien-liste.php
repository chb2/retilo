
<div class="container">
<h1 class="titrepage"><?php echo $view['title'] ;?></h1>

<p> <a href="<?php echo site_url('/gestion/'.$page.'/ajout');?>" class="btn btn-primary">Ajout</a></p>

<table class="table table-responsive table-striped table-bordered">
    <?php foreach ($t as $r): ?>
    <?php 
        $urledit = site_url('gestion/'.$page.'/edit/'.$r->liid);
        $urlsup = site_url('gestion/'.$page.'/sup/'.$r->liid);
    ?>
    <tr>
        <td>
            <a href="<?php echo $urledit;?>">
            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
            </a>  
        </td>
        <td>
            <?php echo $r->liordre;?>
           
        </td>
        <td>
            <?php echo $r->lititre;?>
           
        </td>
        <td>
            <?php echo $r->liurl;?>
           
        </td>
        <td>
            <?php echo $r->livisible;?>
           
        </td>
        <td>
        <?php
            echo "<a href=".$urlsup."><i class='fa fa-times text-danger' aria-hidden='true'></i></a>";
        ?>
        </td>


</tr>
<?php endforeach ?>
</table>
    </div>