<div class="container">
    <h1 class="titrepage"><?= lang('Text.menutitle') ?></h1>
    <p> <a href="<?php echo site_url('/gestion/'.$page.'/ajout/'.$menucode);?>" class="btn btn-primary"><?= lang('Text.add') ?></a></p>

    <table class="table table-responsive table-striped table-bordered">
        <?php foreach ($t as $r): ?>
        <?php 
        $urledit = site_url('gestion/'.$page.'/edit/'.$r->id);
        $urlsup = site_url('gestion/'.$page.'/sup/'.$r->id);
     //   $urldet = site_url('gestion/'.$page.'/edit/'.$r->$id);
        ?>
        <tr>
            <td>
                <a href="<?php echo $urledit;?>">
                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                </a>  
            </td>

            <td>
                <?php echo $r->texte;?>        
            </td>
            <td>     
                <?php echo $r->link;?>   
            </td>

            <td>
                <?php 
                if ($r->disabled == 0)
                {   echo lang('Text.active');}
                else
              
                {   echo lang('Text.disabled');}
             
                ?>        
            </td>

        </tr>
    <?php endforeach ?>
    </table>
</div>

 
