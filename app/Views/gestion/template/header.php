<!DOCTYPE html>
<html lang="en">
    <head>
         <meta charset="utf-8" />
        <?php
        $metatitle = "";
        if (isset($meta['title'])) $metatitle = $meta['title'];
        $metadescription = "";
        if (isset($meta['description'])) $metadescription = $meta['description'];
        ?>
        <title><?php echo $metatitle; ?></title>
        <meta name="description" content="<?php echo $metadescription; ?>" />
    
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <link rel="stylesheet" href="<?php echo base_url('css/bootstrap.min.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('fa/css/fork-awesome.min.css');?>">
        <link rel="stylesheet" href="<?php echo base_url('css/style.css?v0.1.7');?>">

        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- simplemde-->

    <!--    <link rel="stylesheet" href="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.css">*/
       <script src="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.js"></script>
-->
<script src="<?php echo base_url('js/tinymce/tinymce.min.js');?>" referrerpolicy="origin"></script>
        <script>
      tinymce.init({
        relative_urls : false,
        selector: '#textareatinymce',
        plugins: 'link image code',
        toolbar: 'undo redo image link code'
      });
    </script>
            <script src="<?php echo base_url('js/tool/js.js');?>" referrerpolicy="origin"></script>

    </head>
 
    <body>
    <!-- Navigation-->
       


        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div class="container">
                <a class="navbar-brand" href="<?php echo site_url('');?>"><?php echo AA_APPLICATION_NAME ?></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon ml-auto"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div class="navbar-nav">
                        <a class="nav-item nav-link active" href="<?php echo site_url('');?>">Accueil<span class="sr-only">(current)</span></a>
                        <a class="nav-link" href="<?php echo base_url('index.php/gestion/menu');?>">Menu</a>        
                        <li><a class="nav-link" href="<?php echo base_url('index.php/gestion/pagecms');?>">Pages</a> </li> 
                        <li><a class="nav-link" href="<?php echo base_url('index.php/gestion/pagero');?>">Articles</a> </li> 
                        <li class="nav-item active">
                            <a class="nav-link" href="<?php echo site_url('gestion/lien');?>">
                                Liens
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>   

                        <ul class="nav-item dropdown ">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            configuration</a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="<?php echo base_url('index.php/gestion/parametro');?>">Paramètres</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="<?php echo base_url('index.php/gestion/uzanto');?>">Utilisateurs</a>

            
                                <a class="dropdown-item" href="<?php echo base_url('index.php/gestion/komputo');?>">Statistiques</a>
                                <a class="dropdown-item" href="<?php echo base_url('index.php/gestion/utileco');?>">Outils</a>
                                <a class="dropdown-item" href="<?php echo base_url('index.php/gestion/log');?>">Log</a>
                            </div>
                        </ul>
                        <li><a class="nav-link" href="<?php echo base_url('index.php/gestion/login/logout');?>">Deconnexion</a></li>  
 
                    </div>
                </div>
            </div>
        </nav>
