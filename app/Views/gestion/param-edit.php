<div class="container">
<h1 class="titrepage"><?= lang('Text.update')?> <?= lang('Text.parameter')?></h1>
<?php
  $useradmin='A';
helper('form');

if ($action == "add"){ 
    echo form_open_multipart('gestion/'.$page.'/add'); 
    $txtbouton = lang('Text.add');
    $classbouton = "class='btn btn-primary'";
}
if ($action == "upd"){ 
    echo form_open_multipart('gestion/'.$page.'/upd'); 
    $txtbouton = lang('Text.update');
    $classbouton = "class='btn btn-primary'";
}
if ($action == "del"){
     echo form_open_multipart('gestion/'.$page.'/del'); 
     $txtbouton = lang('Text.delete');
     $classbouton = "class='btn btn-danger'";}

?>
<?php
  if (!empty($erreurs)) {
    echo "<br/>";
    ?>
  <div class="alert alert-danger" role="alert">
 <p>Attention il y a au moins une erreur</p>
    <?php foreach ($erreurs as $erreur): ?>
        <li><?php echo $erreur; ?></li>
        <?php endforeach ?>
  </div>
  <?php } ?>
<?php
          echo form_hidden('paty',$r->paty);
?>


    <div class="form-group row">
        <span class="col-sm-2 col-form-label" ><?= lang('Text.code')?></span>
        <div class="col-10">
        <?php

            $data = array(
                        'name'        => 'paco',
                        'type'        => 'text',
                        'value'       =>  $r->paco,
                        'style'       => 'width: 100%'
                        );
            echo form_input($data);
            ?>
        </div>
    </div>
     
    <div class="form-group row">
        <span class="col-sm-2 col-form-label" ><?= lang('Text.label')?></span>
        <div class="col-10">
        <?php

            $data = array(
                        'name'        => 'libelle',
                        'type'        => 'text',
                        'value'       =>  $r->libelle,
                        'style'       => 'width: 100%'
                        );
            echo form_input($data);
            ?>
        </div>
    </div>

         
    <div class="form-group row">
        <span class="col-sm-2 col-form-label" ><?= lang('Text.value')?></span>
        <div class="col-10">
        <?php

            $data = array(
                        'name'        => 'zona',
                        'type'        => 'text',
                        'value'       =>  $r->zona,
                        'style'       => 'width: 100%'
                        );
            echo form_input($data);
            ?>
        </div>
    </div>
 
    <div class="form-group row">
        <span class="col-sm-2 col-form-label" ><?= lang('Text.disabled')?></span>
        <div class="col-10">
        <?php 
        if ($r->disabled == 0){$tf = false;} else {$tf = true; }
            $data = array(
                        'name'        => 'disabled',
                        'checked'     =>  $tf ,
                        'value'       =>  $r->disabled,
                        'style'       => 'width: 2em%'
                        );
            echo form_checkbox($data);
            ?>
        </div>
    </div>
 
 
 
    <?php  if ($action <> "add"){ ?>

<div class="form-group row">
    <span class="col-sm-2 text-secondary"><?= lang('Text.creation')?></span>
    <span class="col-sm-2 text-secondary"><?php echo $r->datcrt;?> </span>
    <span class="col-sm-2 text-secondary"> <?php echo $r->usrcrt;?> </span>
</div>
<div class="row">
    <span class="col-sm-2 text-secondary"><?= lang('Text.update')?></span>
    <span class="col-sm-2 text-secondary"><?php echo $r->datmod;?></span>
    <span class="col-sm-2 text-secondary"> <?php echo $r->usrmod;?> </span>
</div>
    <?php } ?>
 
<div class="form-group row">
    <div class="col-sm-2">
    <?php
    if ($action <> "add"){
    echo form_hidden('id',$r->id);}


    echo form_submit('submit',$txtbouton, $classbouton);

    echo form_close();
    ?>
    </div>
</div>
</div>