<div class="container">
    <h1 class="titrepage"><?php echo $view['title'] ;?></h1>

    <div class="row mx-1">
        <div class="col-6" >
            Page par défaut
        </div>

        <div class="col-1" >
        <?php
         $urledit = site_url('gestion/'.$page.'/edityco/site_defaultpage');
        ?>
        <a href="<?php echo $urledit;?>">
        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
        </a>
 
        </div>
        <div class="col-5" >
            <?php
            echo $site_defaultpage;
        

            ?>
        </div>

    </div>
    <div class="row mx-1">
        <div class="col-6" >
            Compteur (0/1)
        </div>

        <div class="col-1" >
        <?php
         $urledit = site_url('gestion/'.$page.'/edityco/site_komputo');
        ?>
        <a href="<?php echo $urledit;?>">
        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
        </a>
 
        </div>
        <div class="col-5" >
            <?php
            echo $site_komputo;
        

            ?>
        </div>

    </div>



    <div class="row mx-1">
        <div class="col-12" >
        <a  href="<?php echo base_url('index.php/gestion/param');?>">Paramètres avancés</a>
        </div>
         
    </div>
   
</div>