<div class="container">
    <h1 class="titrepage"><?= lang('Text.statistics')?></h1>



    <table class="table table-responsive table-striped table-bordered">
        <?php foreach ($t as $r): ?>
        <?php 
        $urledit = site_url('gestion/'.$page.'/edit/'.$r->id);
        $urlsup = site_url('gestion/'.$page.'/sup/'.$r->id);
        ?>
        <tr>
           <td>
                <a href="<?php echo $urledit;?>">
                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                </a>  
            </td>


            <td>
                <?php echo $r->url; ?>
        
            </td>
            <td>
                <?php echo $r->periodatipo;?>        
            </td>
            <td>
                <?php echo $r->periodavaloro;?>        
            </td>
            <td>
                <?php echo $r->komputavaloro;?>        
            </td>

            <td>     
            <?php
                    echo "<a href=".$urlsup."><i class='fa fa-times text-danger' aria-hidden='true'></i></a>";
                ?>
            </td>

        </tr>
    <?php endforeach ?>
    </table>
</div>

 
