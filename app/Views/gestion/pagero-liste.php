<div class="container">
<h1 class="titrepage"><?php //echo $view['title'];?></h1>

<p> <a href="<?php echo site_url('gestion/'.$page.'/ajout/'.$pageid);?>" class="btn btn-primary">Ajout</a></p>

</p>
<div class="responsive-table-line">
<table class="table table-responsive table-striped table-bordered">

    <?php foreach ($t as $r): ?>
    <?php 
    $index = 'index.php/';
    $urledit = base_url($index.'gestion/'.$page.'/edit/'.$r->id);
    $urlsup = base_url($index.'gestion/'.$page.'/sup/'.$r->id);
    $urlvue = base_url($index.'gestion/'.$page.'/vue/'.$r->id);

    ?>
    <tr class="ttr">
    
        <td class="tda">
            <a href="<?php echo $urledit;?>">
            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
            </a>  
        </td>


        <td class="tda" data-title="">
            <?php echo $r->id;?>
        </td>
        
        <td class="tda" data-title="">
            <?php echo $r->pageid;?>
        </td>
        <td class="tda" data-title="Titre">
            <?php echo $r->titre;?>
        </td>
        <td class="tda" data-title="Texte court">
            <?php echo $r->textecourt;?>
        </td>
 
        <td>     
            <?php
                echo "<a href=".$urlsup."><i class='fa fa-times text-danger' aria-hidden='true'></i></a>";
            ?>
        </td>
       
</tr>
<?php endforeach ?>
</table>
    </div>
</div>

