<div class="container">
<h1 class="titrepage"><?php echo $view['title'] ;?></h1>
<?php
  $useradmin='A';
helper('form');

if ($action == "add"){ 
    echo form_open_multipart('gestion/'.$page.'/add'); 
    $txtbouton = "Ajout";
    $classbouton = "class='btn btn-primary'";
}
if ($action == "upd"){ 
    echo form_open_multipart('gestion/'.$page.'/upd'); 
    $txtbouton = "Mise à jour";
    $classbouton = "class='btn btn-primary'";
}
if ($action == "del"){
     echo form_open_multipart('gestion/'.$page.'/del'); 
     $txtbouton = "Suppression"; 
     $classbouton = "class='btn btn-danger'";}

?>
<?php
  if (!empty($erreurs)) {
    echo "<br/>";
    ?>
  <div class="alert alert-danger" role="alert">
 <p>Attention il y a au moins une erreur</p>
    <?php foreach ($erreurs as $erreur): ?>
        <li><?php echo $erreur; ?></li>
        <?php endforeach ?>
  </div>
  <?php } ?>
 
  
  <div class="form-group row">
        <span class="col-sm-2 col-form-label" >Titre</span>
        <div class="col-10">
            <?php
            $data = array(
                        'name'        => 'lititre',
                        'type'        => 'text',
                        'value'       => $r->lititre,
                        'style'       => 'width: 100%'
                        );
            echo form_input($data);
            ?>
        </div>
    </div>


    <div class="form-group row">
        <span class="col-sm-2 col-form-label" >URL</span>
        <div class="col-10">
            <?php
            $data = array(
                        'name'        => 'liurl',
                        'type'        => 'text',
                        'value'       => $r->liurl,
                        'style'       => 'width: 100%'
                        );
            echo form_input($data);
            ?>
        </div>
    </div>

    <div class="form-group row">
        <span class="col-sm-2 col-form-label" >Ordre</span>
        <div class="col-10">
            <?php
            $data = array(
                        'name'        => 'liordre',
                        'type'        => 'text',
                        'value'       => $r->liordre,
                        'style'       => 'width: 100%'
                        );
            echo form_input($data);
            ?>
        </div>
    </div>
    <div class="form-group row">
        <span class="col-sm-2 col-form-label" >Visible (0/1)</span>
        <div class="col-10">
            <?php
            $data = array(
                        'name'        => 'livisible',
                        'type'        => 'text',
                        'value'       => $r->livisible,
                        'style'       => 'width: 100%'
                        );
            echo form_input($data);
            ?>
        </div>
    </div>

    <div class="form-group row">
        <span class="col-sm-2 col-form-label" >Image</span>
        <div class="col-10">
            <?php
            $data = array(
                        'name'        => 'liimage',
                        'type'        => 'text',
                        'value'       => $r->liimage,
                        'style'       => 'width: 100%'
                        );
            echo form_input($data);
            ?>
        </div>
    </div>
    <div class="form-group row">
        <span class="col-sm-2 col-form-label" >Icone</span>
        <div class="col-10">
            <?php
            $data = array(
                        'name'        => 'liicone',
                        'type'        => 'text',
                        'value'       => $r->liicone,
                        'style'       => 'width: 100%'
                        );
            echo form_input($data);
            ?>
        </div>
    </div>

 
    <?php  if ($action <> "add"){ ?>

<div class="form-group row">
    <span class="col-sm-2 text-secondary">Création </span>
    <span class="col-sm-2 text-secondary"><?php //echo $r->datcrt;?> </span>
    <span class="col-sm-2 text-secondary"> <?php //echo $r->usrcrt;?> </span>
</div>
<div class="row">
    <span class="col-sm-2 text-secondary">Modification </span>
    <span class="col-sm-2 text-secondary"><?php //echo $r->datmod;?></span>
    <span class="col-sm-2 text-secondary"> <?php //echo $r->usrmod;?> </span>
</div>
    <?php } ?>
 
<div class="form-group row">
    <div class="col-sm-2">
    <?php
 
    if ($action <> "add"){
    echo form_hidden('liid',$r->liid);}


    echo form_submit('submit',$txtbouton, $classbouton);

    echo form_close();
    ?>
    </div>
</div>
</div>