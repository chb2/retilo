<div class="container">
    <h1 class="titrepage"><?= lang('Text.tools') ?></h1>
    
    <div class="row">
        <div class="col-6" >
            <p><?= lang('Text.action_reinit_db') ?></p>
         </div> 
        <div class="col-6">
                <?php
    
                $classbouton = "class='btn btn-primary'";
                helper('form');
                $url = base_url('index.php/gestion/utileco/installation_preparation');
                echo form_open($url);
                echo form_submit('submit',lang('Text.prepare_install'), $classbouton);
                echo form_close();
                ?>    
        </div>
    </div>

    <div class="row">
        <div class="col-12" >
        <br/>
        </div> 
    </div>
   
</div>