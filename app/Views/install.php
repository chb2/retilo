<div class="container">
    <h1 class="titrepage">
      <?php echo lang('Text.installation'); ?>
    </h1>


    <?php


if ($action == "install-info"){ 
  echo '<br/><p class="alert alert-primary">'.lang('Text.application_not_installed').' </p>';
  ?>
  <div class="row" >
 
    <div class="col-sm-2" >
    <a class="btn btn-primary"   role="button" href="<?php echo base_url('index.php/install/partie1controle');?>">Installer</a>
    </div>
  </div>
<br/>
 <?php
}

if ($action == "installok"){ 
    echo '<br/><p class="alert alert-info">'.lang('Text.application_installed').' </p>';
    ?>

 

<?php
}

if ($action == "install-crtadmin-edit"){ 
    ?>
   <div class="row" >
    <div class="col-sm-2" >

    </div>
    <div class="col-sm-6 grey-border arrondi" >
    <?php
    if ($msg > ''){
      echo '<br/><p class="alert alert-danger">'.$msg.' </p>';
    }
    ?>
  
    <?php
    helper('form');
    echo form_open("install/partie3installation");
  
  
    echo "<p><label for='Login'>Identifiant</label><br/>";
    $data = array(
        'name'        => 'u',
        'style'     => 'width: 100%'
      );
    echo form_input($data);
  
    ?>
   
  
    <?php
    echo "<p><label for='p'>Mot de passe</label><br/>";
    $data = array(
      'name'      => 'p',
      'style'     => 'width: 100%'
      
  
    );
    echo form_password($data);
    echo "</p>";
    $classbouton = "class='btn-block btn-primary'";
    echo form_submit('submit','Installation',$classbouton);
    echo form_close();
    ?>
    <br/>
    </div>
      <div class="col-sm-2" >
  
      </div>

      </div>
<?php


}

  ?>

</div>