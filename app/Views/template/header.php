<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <?php
        $metatitle = "Application cadre";
        if (isset($meta['title'])) $metatitle = $meta['title'];
        $metadescription = "";
        if (isset($meta['description'])) $metadescription = $meta['description'];
        ?>
        <title><?php echo $metatitle; ?></title>
        <meta name="description" content="<?php echo $metadescription; ?>" />

        
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <link rel="stylesheet" href="<?php echo base_url('css/bootstrap.min.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('/fa/css/fork-awesome.min.css');?>">
        <link rel="stylesheet" href="<?php echo base_url('css/style.css?v0.7');?>">

        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
    </head>
 
    <body>
