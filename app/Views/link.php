<div class="container">
    <h1 class="titrepage"><?php echo $view['title'] ;?></h1>

    <?php
    if ( isset($r->textelong)){

        echo $r->textelong;
    }
    ?>

    <?php
    $c=0; 
    $baseurl = base_url('');
    ?>
    <div class="row">
        <?php  foreach ($l as $rl): ?>
            <div class="col">
                <?php
                    $url = $baseurl.$rl->liurl;
                    if (substr($rl->liurl, 0, 4) == "http"){
                        $url = $rl->liurl;
                    }
                    $codeimage ="";
                    if (substr($rl->liimage, 0, 4) == "http"){
                        $codeimage = '<img src='.$rl->liimage.' class="imgskatalo"> ';
                      }
      
                      if ($rl->liicone > ""){
                          $codeimage = $rl->liicone;
                    }
      
                ?>
                
                <a class="btn btn-primary btn-lg bouton1" href="<?php echo $url; ?>" role="button">
                <div class="bouton1-img">
                    <?php echo $codeimage; ?>
                </div>
                <div class="bouton1texte">
                    <?php echo $rl->lititre; ?>
                </div>

            
            </a>
            </div>
    
        <?php endforeach ?>
    </div>
    <div class="compteur">
    <?php
    if ( isset($cpt)){
        echo $cpt;
        }
    ?>
    </div>
</div>

