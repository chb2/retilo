<?php 
namespace App\Models;
use CodeIgniter\Model;
use App\Models\MenuModel;

class PageModel extends Model{

    /*
    public function affiche($data,$page = 'home')
    {
        $page = $data['page']; 
        if ( ! is_file(APPPATH.'/Views/'.$page.'.php'))
        {
            // Whoops, we don't have a page for that!
            throw new \CodeIgniter\Exceptions\PageNotFoundException($page);
        }


       $data['title'] = ucfirst($page); 
        echo view('template/header', $data);

       
        $menu = new MenuModel();
        $data['menu'] =  $menu->getactifmenus('menu');

        echo view('template/menu.php', $data);
        echo view($page.'.php', $data);
        echo view('template/footer', $data);
    }
*/
    public function affiche($data,$page = 'home')
    {
        $page = $data['page']; 
        if ( ! is_file(APPPATH.'/Views/'.$page.'.php'))
        {
            // Whoops, we don't have a page for that!
            throw new \CodeIgniter\Exceptions\PageNotFoundException($page);
        }


       $data['title'] = ucfirst($page); 
        echo view('template/header', $data);

       
        $menu = new MenuModel();
        $data['menu'] =  $menu->getactifmenus('menu');

        // compteur debut
        $komputo = new KomputoModel();

        if ($page == 'page')
        {
            $p['url']=  'page-'.$data['code'] ;
        }

        if ($page == 'pagero')
        {
            $p['url']=  $data['page'].'-'.$data['id'] ;
        }

        $data['cpt'] = $komputo->add1komputo($p);
         // compteur fin

        echo view('template/menu.php', $data);
        echo view($page.'.php', $data);
        echo view('template/footer', $data);
    }


    public function page($data)
    {
        $page = $data['page']; 
        if ( ! is_file(APPPATH.'/Views/'.$page.'.php'))
        {
            // Whoops, we don't have a page for that!
            throw new \CodeIgniter\Exceptions\PageNotFoundException($page);
        }

        $data['title'] = ucfirst($page); 
        echo view('template/header', $data);


        // compteur debut
        $komputo = new KomputoModel();
        $p['url']=  'page-'.$data['code'] ;
        $data['cpt'] = $komputo->add1komputo($p);
         // compteur fin
         

        if (!isset($data['menu'])) $data['menu'] = '1';
        if ( $data['menu'] == '1' )
        {
            $menu = new MenuModel();
            $data['menu'] =  $menu->getactifmenus('menu');
            echo view('template/menu.php', $data);
        }

        echo view($page.'.php', $data);
        if (!isset($data['pied'])) $data['pied'] = '1';
        if ( $data['pied'] == '1' )
        {
            echo view('template/footer', $data);
        }
      
    }

    public function gestion($data,$page = 'home')
    {

        echo view('gestion/template/header', $data);
        echo view('gestion/'.$page.'.php', $data);
        echo view('gestion/template/footer', $data);
    }

  
}
