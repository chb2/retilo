<?php namespace App\Models;
use CodeIgniter\Model;

class PagecmsModel extends Model
{
    protected $table = 'page';

    public function get1perid($id)
    {
      $db = db_connect();
      $strsql = "SELECT * FROM page where id = ".$id." ";
      $query = $this->db->query($strsql);
      $r = $query->getRow();
      return  $r;
    }


    // par code
    public function get1percode($code)
    {
      $db = db_connect();
      $strsql = "SELECT * FROM page where code = '".$code."' ";
      $query = $this->db->query($strsql);
      $r = $query->getRow();
      return  $r;
      
    }

        // chercher les pages visibles
        public function getpages()
        {
          $db = db_connect();
          $strsql = "SELECT * FROM page where visible <> '0' ";
          $query = $this->db->query($strsql);
          $query = $db->query($strsql);
          return $query->getResult();
        
        }
    
      function CreationTable(){
        echo 'creation table';
        $db = db_connect();

        $DB_TYPE = getenv('DB_TYPE');
        if ( $DB_TYPE == 'SQLite' )
        {
            echo 'sqllite';
            $strsql = " CREATE TABLE `page` (
                `id` INTEGER, 
                `code` TEXT, 
                `titre` TEXT, 
                `textecourt` TEXT, 
                `textelong` TEXT, 
                `image1` TEXT, 
                `metatitle` TEXT, 
                `metadescription` TEXT, 
                `ordre`  INTEGER, 
                `visible` TEXT, 
                `datdeb` TEXT,
                `datfin` TEXT,
                datcrt TEXT,
                usrcrt varchar(30),
                datmod TEXT,
                usrmod varchar(30) NOT NULL ,
            PRIMARY KEY(`id`) ) ";
        }

        if ( $DB_TYPE == 'MySQL' )
        {

            $strsql = "
            CREATE TABLE IF NOT EXISTS `page` (
            `id` int(10) NOT NULL AUTO_INCREMENT,
            `code` varchar(64) NOT NULL,
            `titre` varchar(255) NOT NULL,
            `textecourt` text NOT NULL,
            `textelong` text NOT NULL,    
            `image1` varchar(255) NOT NULL,
            `metatitle` varchar(64) NOT NULL,
            `metadescription` varchar(255) NOT NULL,
            `ordre` tinyint(4) NOT NULL DEFAULT '0',
            `visible` varchar(1) NOT NULL,
            `datdeb`date NOT NULL DEFAULT '2000-01-01',
            `datfin`date NOT NULL DEFAULT '2000-01-01',
            `usrcrt` varchar(40) NOT NULL,
            `datcrt`date NOT NULL DEFAULT '2000-01-01',
            `usrupd` varchar(40) NOT NULL,
            `datupd` date NOT NULL DEFAULT '2000-01-01',
            PRIMARY KEY (`id`)
            ) ENGINE=InnoDB CHARSET=utf8mb4;";
        }
        echo $strsql;
        $db->query($strsql);
    }
   

}
