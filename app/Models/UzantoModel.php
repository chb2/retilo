<?php 
namespace App\Models;
use CodeIgniter\Model;
use App\Models\MailModel;

class UzantoModel extends Model{


    public function get1uzanto($id)
    {
      $db = db_connect();
      $strsql = "SELECT * FROM uzanto where id = ".$id." ";
      $query = $this->db->query($strsql);
      $r = $query->getRow();
      return  $r;
    }


    public function get1uzantoperemail($email)
    {
      $db = db_connect();
      $strsql = "SELECT * FROM uzanto where email = '".$email."' ";
      $query = $this->db->query($strsql);
      $r = $query->getRow();
      return  $r;
    } 

    function getuzantoj($max=100 ){
        // renvoi les acitités d'une catégorie
              $db = db_connect();
              $strsql = "SELECT * FROM uzanto  order by id limit $max";
          
              $query = $db->query($strsql);
              $data['t'] = $query->getResult();
              return  $data['t'];
    }

    function aldoniuzanto($p){
     // $d = $this->addupd('a');
          // $d = $this->addupd('a');
      $p['grpaut'] =  'A' ;
      $p['usrcrt'] =  $p['code'] ;
      $p['datcrt'] = date('Y-m-d H:m');
      $p['usrmod'] =  $p['code'] ;
      $p['datmod'] = date('Y-m-d H:m');
      $db = db_connect();
      $db->table('uzanto')->insert($p);
    } 

    function chgemail($d){
      $data['logcode'] = 'Utilisateur';
      $data['logtext'] = 'Changement email utilisateur '.$d['id']. ' ancien '.$d['emailold'].' nouveau '.$d['email'];
      $log = new LogModel();
      $log->AddLog($data);

      // ajout ancien email dans remarque


      // envoi email avec lien de validation email
      $mail = new MailModel();
      $p['emailTo'] = $d['email'];
      $p['slosilo'] = $d['slosilo'];
      $p['emailSubject'] = 'Validation email pour Monitorado';
      $p['emailMessage'] = 'Merci de valider votre adresse email avec le lien suivant<br/>';
      $id = $d['id'];
      $slosilo = $d['slosilo'];
      $url = site_url('moncompte/validationemail/'.$id.'/'.$slosilo);
      $p['emailMessage'] .= '<br/><a href='.$url.'>'.$url.'</a>';
      
      $mail->send1mail($p);

    }
    function demchgpas2($p){
      $email1=$p['email1'];
    //  echo 'méthode demande de changement de mot de passe'.$email1;;
      $u =  $this->get1uzantoperemail($email1);
      if (isset($u)) {
        $id =  $u->id;
      //  echo 'id'.$id;
      //  echo $u->email;
      //  echo 'email existant : envoi mail création slosilo + mise à jour date limite';
        
        // mise à jour slosilo + date limite
        $this->GenSlosiloChgpw($id);

      // lecture à nouveau pour avoir slosilo
      $u =  $this->get1uzantoperemail($email1);
      $mail = new MailModel();
      $p['emailTo'] = $u->email;
     // $p['slosilo'] = $d['slosilo'];
      $p['emailSubject'] = 'Demande de changement de mot de passe';
      $p['emailMessage'] = 'Un changement de mot de passe a été demandé. <br/>';
      //$id = $p['id'];
      $slosilo = $u->slosilo;
      $url = site_url('moncompte/changementpwd/'.$id.'/'.$slosilo);
      $p['emailMessage'] .= '<br/><a href='.$url.'>'.$url.'</a>';
  
      $mail->send1mail($p);
        

      }
      else
      {
        echo 'email non existant';
        $data['msgerr'] = 'erreur';   
      }

    /* if ($p['email1'] <> $p['email2'])
      {
        $data['msgerr'] = 'Les Identification incorrect';   

      }*/
      // controle si les 2 emails sont idendituq
      // controle de l'existance email
      // envoi lien si ok
    }

    function GenSlosiloChgpw($id){
      // génération slosilo pour une demande de mot de passe 
        $dl = date("Y-m-d H:i:s", strtotime('+72 hours'));
        $d['datlimslo'] = $dl;
        $d['slosilo'] = md5($dl);
        $db = db_connect();
        $builder = $db->table('uzanto');
        $builder->where('id', $id);
        $builder->update($d);
    }
    function CreationTable(){
      // création table utilisateur simple
      $db = db_connect();

      $DB_TYPE = getenv('DB_TYPE');
      if ( $DB_TYPE == 'SQLite' )
      {
        $strsql = " CREATE TABLE IF NOT EXISTS `uzanto` (
          `id` INTEGER, 
          `code` TEXT, 
          `libelle` TEXT, 
          `email` TEXT, 
          `pasvor` TEXT, 
          `grpaut` TEXT, 
          datvalemail TEXT,
          slosilo TEXT,
          datlimslo TEXT,
          remarque text  ,
          datcrt TEXT,
          usrcrt varchar(30),
          datmod TEXT,
          usrmod varchar(30) NOT NULL ,
      PRIMARY KEY(`id`) ) ";


      }

      if ( $DB_TYPE == 'MySQL' )
      {
      $strsql = "CREATE TABLE IF NOT EXISTS uzanto (
          id int(8) NOT NULL AUTO_INCREMENT,
          code varchar(255) NOT NULL,
          libelle varchar(255) NOT NULL,
          email varchar(255) NOT NULL,
          pasvor varchar(255) NOT NULL,
          grpaut varchar(1) NOT NULL COMMENT 'Groupe autorisation (A=Admin)',
          datvalemail date NOT NULL DEFAULT '2001-01-01' COMMENT 'Date de validation email',
          slosilo varchar(255) COMMENT 'clé pour contrôle',
          datlimslo datetime NOT NULL DEFAULT '2001-01-01 00:00:00' COMMENT 'Date limite pour la clé',
          remarque text  ,
          datcrt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
          usrcrt varchar(30) NOT NULL ,
          datmod datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
          usrmod varchar(30) NOT NULL ,
          PRIMARY KEY (id)
          ) ENGINE=InnoDB CHARSET=utf8mb4;";
      }
      if (isset($strsql))
      {

          $db->query($strsql);

          $data['logcode'] = 'installation';
          $data['logtext'] = 'requete '.$strsql.'  ' ;
          $log = new LogModel();
          $log->AddLog($data);

      }


  }
  function addremarquefield(){
      $db = db_connect();
      $strsql = " ALTER TABLE `uzanto` ADD `remarque` TEXT NOT NULL AFTER `datlimslo`; ";
      echo $strsql;
      $db->query($strsql);

  }


        
}