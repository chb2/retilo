<?php 
namespace App\Models;
use CodeIgniter\Model;

class ToolModel extends Model{

  protected $db;

  public function __construct()
  {
      //$this->db = db_connect();

  }

    public function epuration()
    {
      $db = db_connect();
      // Epuration log
      $ok = 0;

      // épuration log  

      $nbjretention1 = 60;
      $strsql = " delete from `log`  where datcrt < datetime('now','-".$nbjretention1." day','localtime') limit 1000";
      $db->query($strsql);

      $data['logcode'] = 'purge';
      $data['logtext'] = 'purge semajna   '.$strsql.'  ' ;
      $log = new LogModel();
      $log->AddLog($data);

      $ok = 1;
      return $ok;

    }

    public function cleansession()
    {
      // suppression des fichier ci_session plus vieux que 3 jours
      $dateborne = date('Y-m-d', strtotime('-3 day', time()));
      $path = WRITEPATH.'session';

      if (is_dir($path) )
      {
        $nb = 0;
        $max = 1000;
        $handle=opendir($path);
        while (false!==($file = readdir($handle)))
         {
          if ($file != "." && $file != ".."  && $file != "index.html") { 
            $datefichier =  date ("Y-m-d", filemtime("$path/$file"));
            if ($datefichier < $dateborne and $nb < $max)
            {
              $nb = $nb +1;
           //  echo '<br/>'.$file;
            /*  echo 'on peut supprimer';
            echo '<br>'.$dateborne;
            echo '<br>'.$datefichier;*/
            unlink("$path/$file");
           // sleep(1);
            
            }
          }

        }
      }
 
      $data['logcode'] = 'purge';
      $data['logtext'] = 'nettoyage session   ' ;
      $log = new LogModel();
      $log->AddLog($data);

    }

    public function checkfield($table,$field){
      $db = db_connect();
      $strsql = "SELECT count(*) AS nb FROM pragma_table_info('".$table."') WHERE name='".$field."' ";
    //  echo $strsql;
      $query = $db->query($strsql);
      $r = $query->getRow();
      return  $r->nb;
  
  }

    public function addfield($table,$field, $type){
      $db = db_connect();
      if ($this->checkfield($table,$field) > 0)
      {
        echo "<br/>table $table field $field exists";    
      }
      else
      {
        echo "<br/&>table $table field $field do not exist";   
        $strsql = "ALTER TABLE ".$table."  ADD $field $type";
        echo '<br/>'.$strsql;
        try {	 $db->query($strsql); }
         catch (\Exception $e)	 { 	 echo "<br/>Column $field already exists ";	 }
      }


  }


  public function dropfield($table,$field){
    $db = db_connect();
    if ($this->checkfield($table,$field) > 0)
    {
      echo "<br/>table $table field $field exists";   
      $strsql = "ALTER TABLE ".$table."  DROP COLUMN $field ";
      echo '<br/>'.$strsql;
      $query = $db->query($strsql);

    }
    else
    {
      echo "<br/>table $table field $field do not exist";  

    }
 
    
  }          
}