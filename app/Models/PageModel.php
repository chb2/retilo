<?php 
namespace App\Models;
use CodeIgniter\Model;
use App\Models\MenuModel;

class PageModel extends Model{

    public function affiche($data,$page = 'home')
    {
        $data['page'] = $page;
        $this->page($data);
        /*
        // si possible passer tout à page
        $page = $data['page'];        
        if ( ! is_file(APPPATH.'/Views/'.$page.'.php'))
        {
            throw new \CodeIgniter\Exceptions\PageNotFoundException($page);
        }
        $data['title'] = ucfirst($page); 
        echo view('template/header', $data);

       
        $menu = new MenuModel();
        $data['menu'] =  $menu->getactifmenus('menu');

        // compteur debut
        $komputo = new KomputoModel();
      // # $p['url']=  'page-'.$data['code'] ;
       if (array_key_exists('id', $data))
       {
      //  echo 'id défini'.$data['id'];
        $p['url']=  $data['page'].'-'.$data['id'];
       }
       else
       {
     //  echo 'id non défini';
        $p['url']=  $data['page'];


       }
      //exit;
        $p['url']=  'page-'.$data['code'] ;
        $data['cpt'] = $komputo->add1komputo($p);
   

         // compteur fin

        echo view('template/menu.php', $data);
       echo view($page.'.php', $data);
       echo view('template/footer', $data);*/
    }


    public function page($data)
    {
        if (!isset($data['code'])) $data['code'] = '';
        $page = $data['page']; 
        if ( ! is_file(APPPATH.'/Views/'.$page.'.php'))
        {
            throw new \CodeIgniter\Exceptions\PageNotFoundException($page);
        }
        $data['title'] = ucfirst($page); 
        echo view('template/header', $data);


        // compteur debut
        $komputo = new KomputoModel();
        $p['url']=  'page-'.$data['code'] ;
        $data['cpt'] = $komputo->add1komputo($p);

         // compteur fin
         

        if (!isset($data['menu'])) $data['menu'] = '1';
        if ( $data['menu'] == '1' )
        {
            $menu = new MenuModel();
            $data['menu'] =  $menu->getactifmenus('menu');
            echo view('template/menu.php', $data);
        }


        echo view($page.'.php', $data);
        if (!isset($data['pied'])) $data['pied'] = '1';
        if ( $data['pied'] == '1' )
        {
            echo view('template/footer', $data);
        }
      
    }

    public function gestion($data,$page = 'home')
    {

       // a remplacer par page en paramétran le répertoire
       
        $session = \Config\Services::session();
        $userapp = $session->userapp;
        $lang= $session->lang ;
   //     $lang = 'en';
        $language = \Config\Services::language();
        $language->setLocale($lang); 
        // fin code à ajouter dans construc pour langue
       // $locale = 'en';
       // echo 'lang '.$lang;
       // exit;

        $session = \Config\Services::session();
        $userapp = $session->userapp;

        if($userapp == false || $userapp  = NULL || empty($userapp ))
        {
            echo view('template/header', $data);
            
        }
        else
        {
           echo view('gestion/template/header', $data);

        }
        echo view('gestion/'.$page.'.php', $data);
        echo view('gestion/template/footer', $data);
    }

  
}
