<?php 
namespace App\Models;
use CodeIgniter\Model;

class InstallModel extends Model{
  protected $db;

  public function __construct()
  {
      $this->db = db_connect();
  }

  public function installchk()
  {
    // pour l'instant controle si uzanto existe. Aprés on va pouvoir faire des controles plus poussé comme existence de toutes les tables et d'un utilisateur admin
    $ok = 0;
    // $tables = $this->db->list_tables();
  }

  public function install_preparation()
  {
    $data['logcode'] = 'install';
    $nun = date('Y-m-d H:m');

    $strsql = "SELECT name FROM sqlite_master WHERE type='table' ORDER BY name";
    $db = db_connect();
    $query = $db->query($strsql);
    $tables = $query->getResult();
    foreach($tables as $t)
      {
        $table = $t->name;
        $strsql = "delete from $table;";
        $this->db->query($strsql);
        $data['logtext'] = 'Table '.$table.' vidée.' ;
      }
      // page par défaut
      $strsql = "insert into page (code,usrmod) values('home','crt');";
      $this->db->query($strsql);
      // param par défaut

      $this->init_1param('param','site','','');
      $this->init_1param('site','defaultpage','Page par défaut','home');
      $this->init_1param('site','komputo','Compteur','0');
      $this->init_1param('param','typepage','Type de page','');
      $this->init_1param('typepage','blog','Blog','');
      
      // utilisateur par défaut
      $pv = md5('admin');
      $strsql = "insert into uzanto (code,libelle,email,pasvor,datcrt,usrcrt,datmod,usrmod) ";
      $strsql .= " values('admin','admin','','".$pv."','".$nun."','crt','".$nun."','crt')";
      $this->db->query($strsql);

    }

    public function init_1param($paty,$paco,$libelle,$zona)
    {
      $db = db_connect();
      $nun = date('Y-m-d H:m');
      $strsql = "insert into param (paty,paco,libelle,zona,datcrt,usrcrt,datmod,usrmod) ";
      $strsql .= " values('".$paty."','".$paco."','".$libelle."','".$zona."','".$nun."','crt','".$nun."','crt');";
     // echo $strsql;
      $this->db->query($strsql);
    }
        
}
