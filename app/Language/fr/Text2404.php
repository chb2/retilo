<?php
/**
 *
 * Blog - French / Français
 *
 */
return [
    'admin' => "Gestion",
    'uzantotitle' => "Utilisateurs",
    'admin' => "Gestion",
    'add' => "Ajout",
    'language' => "langue",
    'userid' => "Code utilisateur",
    'username' => "Nom d'utilisateur",
    'email' => "Courriel",
    'password' => "Mot de passe",
    'a_admin' => "Administreur A=Admin",
    'remark' => "Remarque",
    'creation' => "Création",
    'update' => "Mise à jour",
    'delete' => "Suppression",
    'tool' => "Outil",
    'tools' => "Outils",
    'action_reinit_db' => "Cette action va réinitialiser la base de données",
    'prepare_install' => "Preparer installation",
    'parameters' => "Paramètres",
    'defaultpage' => "Page par défaut",
    'counter' => "Compteur",
    'defaultpage' => "Page par défaut",
    'advancedsettings' => "Paramètres avancés",
    'useradmin' => "Administrateur",
    'advancedsettings' => "Paramètres avancés",
    'ok' => "OK",
    'install-creation-admin' => "Installation : création d'un compte administrateur",


   

 


];
